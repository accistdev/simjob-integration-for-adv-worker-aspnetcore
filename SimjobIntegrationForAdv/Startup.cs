using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SimjobIntegrationForAdv.Configurations;
using SimjobIntegrationForAdv.Data;
using SimjobIntegrationForAdv.Data.Repositories;
using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Services.Interfaces;
using SimjobIntegrationForAdv.Services.Simjob;
using SimjobIntegrationForAdv.Workers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Data.Repositories.Simjob;

namespace SimjobIntegrationForAdv
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           // services.AddControllers().AddNewtonsoftJson(opt => { opt.SerializerSettings.ReferenceLoopHandling =  Newtonsoft.Json.ReferenceLoopHandling.Ignore; }) ;

            SimjobConfigurations simjobConfiguration = new SimjobConfigurations();
            new ConfigureFromConfigurationOptions<SimjobConfigurations>(
                  Configuration.GetSection("SimjobConfigurations"))
              .Configure(simjobConfiguration);
            services.AddSingleton(simjobConfiguration);

            WorkerConfigurations workerConfigurations = new WorkerConfigurations();
            new ConfigureFromConfigurationOptions<WorkerConfigurations>(
                  Configuration.GetSection("WorkerConfigurations"))
              .Configure(workerConfigurations);
            services.AddSingleton(workerConfigurations);

            OSConfigurations OSConfigurations = new OSConfigurations();
            new ConfigureFromConfigurationOptions<OSConfigurations>(
                  Configuration.GetSection("OSConfigurations"))
              .Configure(OSConfigurations);
            services.AddSingleton(OSConfigurations);

            services.AddHttpClient("simjob", c =>
            {
                c.BaseAddress = new Uri(simjobConfiguration.Api);
                
            });
            services.AddCors(options => options.AddPolicy("All", cp => cp.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().Build()));
            
            services.AddScoped<IClienteDatabaseRepository, ClienteRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();

            services.AddScoped<IProdutoDatabaseRepository, ProdutoRepository>();
            services.AddScoped<IOSDatabaseRepository, OSRepository>();
            services.AddScoped<IResourceRepository, ResourceRepository>();
            services.AddScoped<IStageTypeRepository, StageTypeRepository>();
            services.AddScoped<IItemRepository, ItemRepository>();
            services.AddScoped<IUnitRepository, UnitRepository>();
            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<IEnvironmentRepository, EnvironmentRepository>();

            services.AddScoped<IAccountIntegracaoService, AccountIntegracaoService>();
            services.AddScoped<IItemIntegracaoService, ItemIntegracaoService>();
            services.AddScoped<IJobIntegracaoService, JobIntegracaoService>();

            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<ContextDatabase>();

            services.AddHostedService<InsereClientesSimjobWorker>();
            services.AddHostedService<InsereProdutosSimjobWorker>();
            services.AddHostedService<InsereJobsSimjobWorker>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("All");

            //app.UseRouting();

            //app.UseAuthorization();
            //app.UseAuthentication();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});
        }
    }
}
