﻿using FirebirdSql.Data.FirebirdClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Data
{

    public class ContextDatabase
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public FbConnection GetConnection
        {
            get
            {
                return new FbConnection(_connectionString);
            }
        }

        public ContextDatabase(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("Firebird");
        }
    }
}
