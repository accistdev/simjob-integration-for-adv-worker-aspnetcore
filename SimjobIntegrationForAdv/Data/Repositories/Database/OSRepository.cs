﻿using Dapper;
using Serilog;
using SimjobIntegrationForAdv.Configurations;
using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimjobIntegrationForAdv.Data.Repositories
{
    public class OSRepository : IOSDatabaseRepository
    {
        protected readonly ContextDatabase context;
        private readonly OSConfigurations _osConfigurations;
        public OSRepository(ContextDatabase context, OSConfigurations osConfigurations)
        {
            this.context = context;
            _osConfigurations = osConfigurations;
        }

        public List<OS> GetAll()
        {
            try
            {
                string query = @"SELECT 
                                    o.OS CODOS ,o.CODCLI, o.PREVENTREGA ,o.DATAOS ,p.CODPRO,p.CODIGO CodigoProduto , p.DESCRICAO Produto,s.DESCRICAO status
                                    FROM OS o 
                                    JOIN OSITENSPRO ip ON o.CODOS = ip.CODOS 
                                    JOIN PRODUTOS p ON p.CODPRO = ip.CODPRO 
                                    JOIN status s ON o.CODSTATUS  = s.CODSTATUS 
                                    ";
                using (var conn = context.GetConnection)
                {
                    var res = conn.Query<OS>(query);

                    return res.ToList();
                }
            }
            catch (Exception e)
            {
                Log.Error(e, e.Message);
                return null;
            }
        }

    }
}
