﻿using SimjobIntegrationForAdv.Models.Database;
using System.Collections.Generic;

namespace SimjobIntegrationForAdv.Data.Repositories.Interfaces
{
    public interface IProdutoDatabaseRepository 
    {
        List<Produto> GetAllProdutos();
        Produto GetByCodPro(string codPro);
    }
}
