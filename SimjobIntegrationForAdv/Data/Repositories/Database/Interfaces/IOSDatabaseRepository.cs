﻿using SimjobIntegrationForAdv.Models.Database;
using System.Collections.Generic;

namespace SimjobIntegrationForAdv.Data.Repositories.Interfaces
{
    public interface IOSDatabaseRepository
    {
        List<OS> GetAll();
    }
}
