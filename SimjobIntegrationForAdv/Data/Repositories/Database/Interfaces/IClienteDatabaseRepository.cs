﻿using SimjobIntegrationForAdv.Models.Database;
using System.Collections.Generic;

namespace SimjobIntegrationForAdv.Data.Repositories.Interfaces
{
    public interface IClienteDatabaseRepository
    {
        List<ClienteDatabase> GetAllClientes();
    }
}
