﻿using Dapper;
using Serilog;
using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Data.Repositories
{
    public class ProdutoRepository : RepositoryDatabase,IProdutoDatabaseRepository
    {
        public ProdutoRepository(ContextDatabase context) : base(context)
        {
        }

        public Produto GetByCodPro(string codPro)
        {
            try
            {
                string query = $"SELECT CODIGO, CODPRO , DESCRICAO , UN FROM PRODUTOS p WHERE CODPRO = '{codPro}'";
                using (var conn = context.GetConnection)
                {
                    var res = conn.Query<Produto>(query);
                    return res.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Log.Error(e, e.Message);
                return null;
            }
        }
        public List<Produto> GetAllProdutos()
        {
            try
            {
                string query = "SELECT CODIGO, CODPRO , DESCRICAO , UN FROM PRODUTOS p ";
                using (var conn = context.GetConnection)
                {
                    var res = conn.Query<Produto>(query);
                    return res.ToList();
                }
            }
            catch (Exception e)
            {
                Log.Error(e, e.Message);
                return null;
            }
        }
    }
}
