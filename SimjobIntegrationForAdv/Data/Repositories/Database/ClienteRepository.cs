﻿using Dapper;
using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Models.Database;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimjobIntegrationForAdv.Data.Repositories
{
    public class ClienteRepository : IClienteDatabaseRepository
    {
        protected readonly ContextDatabase context;
        public ClienteRepository(ContextDatabase context)
        {
            this.context = context;
        }

        public List<ClienteDatabase> GetAllClientes()
        {
            try
            {
                string query = "select CODCLI,RAZAO from clientes";
                using (var conn = context.GetConnection)
                {
                    var res = conn.Query<ClienteDatabase>(query);
                    return res.ToList();
                }
            }catch(Exception e)
            {
                Log.Error(e, e.Message);
                return null;
            }
        }
    }
}
