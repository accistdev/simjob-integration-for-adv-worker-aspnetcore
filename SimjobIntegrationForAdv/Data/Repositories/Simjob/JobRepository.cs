﻿using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob
{
    public class JobRepository : RepositorySimjob<Job>, IJobRepository
    {
        public JobRepository(ITokenService tokenService) : base("job",tokenService)
        {
        }

    }
}
