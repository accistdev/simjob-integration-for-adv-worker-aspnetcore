﻿using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;
using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob
{
    public class AccountRepository : RepositorySimjob<Account>, IAccountRepository
    {
        public AccountRepository( ITokenService tokenService) : base("account", tokenService)
        {
        }
    }
}
