﻿using Newtonsoft.Json;
using Serilog;
using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Models;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob
{
    public class RepositorySimjob<T> : ISimjobRepository<T> where T : SimjobEntity
    {
        protected readonly ITokenService tokenService;
        protected readonly string routeApi;

        public RepositorySimjob(string routeApi, ITokenService tokenService)
        {
            this.tokenService = tokenService;
            this.routeApi = routeApi;
        }

        public virtual async Task Insert(List<T> entities)
        {
            var client = await tokenService.GetTokenToHttpClient();

            foreach (var item in entities)
            {
                try
                {

                    string json = JsonConvert.SerializeObject(item);
                    StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                    var res = await client.PostAsync(routeApi, content);
                    if (!res.IsSuccessStatusCode)
                    {
                        var resString = await res.Content.ReadAsStringAsync();
                        Log.Error($"Bad Request: POST Route {this.routeApi} Msg: {resString}");
                        continue;
                    }
                   
                }
                catch (Exception e)
                {
                    Log.Error(e, e.Message);
                    continue;
                }
            }
        }


        public virtual async Task<List<T>> GetAll()
        {
            try
            {
                var client = await tokenService.GetTokenToHttpClient();
                if (client == null) return null;

                var res = await client.GetAsync(routeApi);
                if (!res.IsSuccessStatusCode) return null;

                var body = await res.Content.ReadAsStringAsync();
                var response = JsonConvert.DeserializeObject<ResponseModel<ResponsePaginationData<List<T>>>>(body);
                return response.Data.Data;
            }
            catch (Exception e)
            {
                Log.Error(e, e.Message);
                return null;
            }

        }
    }
}
