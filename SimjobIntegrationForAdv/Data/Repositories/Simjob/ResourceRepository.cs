﻿using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob
{
    public class ResourceRepository : RepositorySimjob<Resource>, IResourceRepository
    {
        public ResourceRepository(ITokenService tokenService) : base("resource", tokenService)
        {
        }
    }
}
