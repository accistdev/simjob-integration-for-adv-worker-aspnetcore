﻿
using SimjobIntegrationForAdv.Models.Simjob;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces
{
    public interface IEnvironmentRepository : ISimjobRepository<Environment>
    {
    }
}
