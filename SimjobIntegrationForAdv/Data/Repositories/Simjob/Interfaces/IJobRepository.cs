﻿using SimjobIntegrationForAdv.Models.Simjob;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces
{
    public interface IJobRepository : ISimjobRepository<Job>
    {

    }
}
