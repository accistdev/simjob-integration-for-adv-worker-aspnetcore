﻿using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;

namespace SimjobIntegrationForAdv.Data.Repositories.Interfaces
{
    public interface IStageTypeRepository : ISimjobRepository<StageType>
    {
    }
}
