﻿using SimjobIntegrationForAdv.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces
{
    public interface ISimjobRepository<T> where T : SimjobEntity
    {
        Task<List<T>> GetAll();
        Task Insert(List<T> accounts);
    }
}
