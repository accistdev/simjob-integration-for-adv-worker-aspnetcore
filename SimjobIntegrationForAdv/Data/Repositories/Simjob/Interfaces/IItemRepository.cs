﻿using SimjobIntegrationForAdv.Models.Simjob;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces
{
    public interface IItemRepository : ISimjobRepository<Item>
    {
    }
}
