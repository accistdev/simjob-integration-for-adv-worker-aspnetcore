﻿using SimjobIntegrationForAdv.Models.Simjob;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces
{
    public interface IAccountRepository : ISimjobRepository<Account>
    {

    }
}
