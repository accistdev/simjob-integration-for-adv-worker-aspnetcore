﻿using SimjobIntegrationForAdv.Models.Simjob;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces
{
    public interface IUnitRepository : ISimjobRepository<Unit>
    {

    }
}
