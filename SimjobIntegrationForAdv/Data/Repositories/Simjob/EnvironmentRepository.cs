﻿
using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob
{
    public class EnvironmentRepository : RepositorySimjob<Environment>,IEnvironmentRepository
    {
        public EnvironmentRepository(ITokenService tokenService) : base("environment", tokenService)
        {
        }

    }
}
