﻿using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob
{
    public class UnitRepository : RepositorySimjob<Unit>, IUnitRepository
    {
        public UnitRepository(ITokenService tokenService) : base("unit",tokenService)
        {
        }
    }
}
