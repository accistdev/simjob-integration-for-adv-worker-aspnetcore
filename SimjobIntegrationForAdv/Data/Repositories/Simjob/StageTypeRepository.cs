﻿using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob
{
    public class StageTypeRepository : RepositorySimjob<StageType>,IStageTypeRepository
    {
        public StageTypeRepository(ITokenService tokenService) : base("stageType", tokenService)
        {
        }
    }
}
