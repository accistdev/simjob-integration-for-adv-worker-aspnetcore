﻿using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;

namespace SimjobIntegrationForAdv.Data.Repositories.Simjob
{
    public class ItemRepository : RepositorySimjob<Item>, IItemRepository
    {

        public ItemRepository(ITokenService tokenService) : base("item",tokenService)
        {

        }


    }
}
