﻿using SimjobIntegrationForAdv.Services.Interfaces;

namespace SimjobIntegrationForAdv.Data.Repositories
{
    public class RepositorySimjob
    {
        protected readonly ITokenService tokenService;

        public RepositorySimjob(ITokenService tokenService)
        {
            this.tokenService = tokenService;
        }
    }
}
