﻿using SimjobIntegrationForAdv.Services.Simjob;
using System.Collections.Generic;

namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class Environment : SimjobEntity
    {
        public string Description { get;  set; }
        public List<JobStageStatus> JobStageStatus { get; set; }
    }
}
