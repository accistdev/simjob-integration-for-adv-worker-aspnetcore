﻿namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class Resource : SimjobEntity
    {
        public string Code { get;  set; }
        public string Description { get;  set; }
        public string Type { get;  set; }
    }
}
