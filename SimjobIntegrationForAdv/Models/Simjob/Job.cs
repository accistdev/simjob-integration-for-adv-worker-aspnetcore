﻿using System;
using System.Collections.Generic;

namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class Job : SimjobEntity
    {
        public Job()
        {

        }

        public Job(string code, string description, DateTime? deadline, string accountId, string environmentId, string itemId, List<JobStage> jobStages = null)
        {
            Code = code;
            Description = description;
            Deadline = deadline;
            AccountId = accountId;
            EnvironmentId = environmentId;
            ItemId = itemId;
            JobStages = jobStages ?? new List<JobStage>();
        }

        public string Code { get;  set; }
        public string Description { get;  set; }
        public DateTime? Deadline { get;  set; }
        public string AccountId { get;  set; }
        public string EnvironmentId { get; set; }
        public string ItemId { get; set; }
        public List<JobStage>  JobStages { get; set; }
    }
}
