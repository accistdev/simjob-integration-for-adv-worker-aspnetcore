﻿namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class TokenContainer
    {
        public string AccessToken { get; set; }
    }
}
