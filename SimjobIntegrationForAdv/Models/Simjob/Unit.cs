﻿using System;

namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class Unit : SimjobEntity
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
    }
}
