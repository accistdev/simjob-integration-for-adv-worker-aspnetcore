﻿using System;

namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class JobStage : SimjobEntity
    {
        public JobStage()
        {

        }
        public JobStage(string description, string status, string resourceId, string stageTypeId)
        {
            Description = description;
            Status = status;
            ResourceId = resourceId;
            StageTypeId = stageTypeId;
        }

        public string JobId { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string ResourceId { get; set; }
        public string StageTypeId { get; set; }
    }
}
