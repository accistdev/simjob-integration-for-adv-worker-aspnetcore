﻿namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class StageType : SimjobEntity
    {
        public string Description { get; set; }
    }
}
