﻿namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class ResponsePaginationData<T>
    {
        public T Data { get; set; }
        public long Total { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
    }
}
