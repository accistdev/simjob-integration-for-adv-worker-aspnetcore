﻿namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class ResponseModel<T> 
    {
        public bool Success { get; set; }
        public T Data { get; set; }

    }
}
