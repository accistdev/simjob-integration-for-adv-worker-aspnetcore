﻿using System;

namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class Item : SimjobEntity
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string UnitId { get; set; }
    }
}
