﻿namespace SimjobIntegrationForAdv.Models.Simjob
{
    public class Account : SimjobEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
