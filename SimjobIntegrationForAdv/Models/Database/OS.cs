﻿using System;

namespace SimjobIntegrationForAdv.Models.Database
{
    public class OS
    {
        public string StatusSimjob { get; set; }
        public string CodigoProduto { get; set; }
        public string CodOs { get; set; }
        public string CodCli { get; set; }
        public string CodPro { get; set; }
        public string Produto { get; set; }
        public string Status { get; set; }
        public DateTime? PrevEntrega { get; set; }
        public DateTime? DataOs { get; set; }
    }
}
