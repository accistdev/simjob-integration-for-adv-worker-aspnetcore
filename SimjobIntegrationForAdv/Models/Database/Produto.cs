﻿namespace SimjobIntegrationForAdv.Models.Database
{
    public class Produto
    {
        public string Codigo { get; set; }
        public string CodPro { get; set; }
        public string Descricao { get; set; }
        public string Un { get; set; }
    }
}
