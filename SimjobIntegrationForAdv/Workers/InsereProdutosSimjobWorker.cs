﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using SimjobIntegrationForAdv.Configurations;
using SimjobIntegrationForAdv.Services.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Workers
{
    public class InsereProdutosSimjobWorker : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly WorkerConfigurations _workerConfigurations;

        public InsereProdutosSimjobWorker(WorkerConfigurations workerConfigurations, IServiceScopeFactory serviceScopeFactory)
        {
            _workerConfigurations = workerConfigurations;
            _serviceScopeFactory = serviceScopeFactory;

        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int intervalo_segundos = _workerConfigurations.InsereProdutosSimjobWorker_Intervalo_Segundos;

            if(intervalo_segundos == 0)
            {
                Log.Information("InsereProdutosSimjob Parado".ToUpper());
                return;
            }

            while (!stoppingToken.IsCancellationRequested)
            {

                var serviceProvider = _serviceScopeFactory.CreateScope().ServiceProvider;
                var itemIntegracaoService = serviceProvider.GetService<IItemIntegracaoService>();

                Log.Information("Executando InsereProdutosSimjob".ToUpper());
                await itemIntegracaoService.InsereItemsNoSimjob();

                Log.Information("Finalizado InsereProdutosSimjob".ToUpper());

                await Task.Delay((intervalo_segundos * 1000), stoppingToken);
            }
        }
    }
}
