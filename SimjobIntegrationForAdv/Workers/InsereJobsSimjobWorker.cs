﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using SimjobIntegrationForAdv.Configurations;
using SimjobIntegrationForAdv.Services.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Workers
{
    public class InsereJobsSimjobWorker : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly WorkerConfigurations _workerConfigurations;

        public InsereJobsSimjobWorker(WorkerConfigurations workerConfigurations, IServiceScopeFactory serviceScopeFactory)
        {
            _workerConfigurations = workerConfigurations;
            _serviceScopeFactory = serviceScopeFactory;

        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int intervalo_segundos = _workerConfigurations.InsereJobsSimjobWorker_Intervalo_Segundos;

            if (intervalo_segundos == 0)
            {
                Log.Information("InsereJobsSimjob Parado".ToUpper());
                return;
            }

            while (!stoppingToken.IsCancellationRequested)
            {

                var serviceProvider = _serviceScopeFactory.CreateScope().ServiceProvider;
                var jobIntegracaoService = serviceProvider.GetService<IJobIntegracaoService>();

                Log.Information("Executando InsereJobsSimjob".ToUpper());
                await jobIntegracaoService.InsereJobsNoSimjob();

                Log.Information("Finalizado InsereJobsSimjob".ToUpper());

                await Task.Delay((intervalo_segundos * 1000), stoppingToken);
            }
        }
    }
}
