﻿using SimjobIntegrationForAdv.Configurations;
using SimjobIntegrationForAdv.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System.Threading;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Workers
{
    public class InsereClientesSimjobWorker : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly WorkerConfigurations _workerConfigurations;

        public InsereClientesSimjobWorker(WorkerConfigurations workerConfigurations, IServiceScopeFactory serviceScopeFactory)
        {
            _workerConfigurations = workerConfigurations;
            _serviceScopeFactory = serviceScopeFactory;
           
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int intervalo_segundos = _workerConfigurations.InsereClientesSimjobWorker_Intervalo_Segundos;

            if (intervalo_segundos == 0)
            {
                Log.Information("InsereProdutosSimjob Parado".ToUpper());
                return;
            }

            while (!stoppingToken.IsCancellationRequested)
            {
             
                var serviceProvider = _serviceScopeFactory.CreateScope().ServiceProvider;
                var accountIntegracaoService = serviceProvider.GetService<IAccountIntegracaoService>();

                Log.Information("Executando InsereClientesSimjob");
                await accountIntegracaoService.InsereClientesNoSimjob();

                Log.Information("Finalizado InsereClientesSimjob");

                await Task.Delay((intervalo_segundos * 1000), stoppingToken);
            }
        }
    }
}
