﻿using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Services.Interfaces
{
    public interface IItemIntegracaoService
    {
        Task InsereItemsNoSimjob();
    }
}
