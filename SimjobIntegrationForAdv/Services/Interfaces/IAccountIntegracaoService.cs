﻿using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Services.Interfaces
{
    public interface IAccountIntegracaoService
    {
        Task InsereClientesNoSimjob();
    }
}
