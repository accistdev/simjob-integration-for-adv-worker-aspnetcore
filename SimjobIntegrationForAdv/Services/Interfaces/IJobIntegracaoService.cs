﻿using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Services.Interfaces
{
    public interface IJobIntegracaoService
    {
        Task InsereJobsNoSimjob();
    }
}
