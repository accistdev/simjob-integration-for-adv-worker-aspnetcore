﻿using SimjobIntegrationForAdv.Models.Simjob;
using System.Net.Http;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Services.Interfaces
{
    public interface ITokenService
    {
        Task<TokenContainer> GetToken();
        Task<HttpClient> GetTokenToHttpClient();
    }
}
