﻿using System;

namespace SimjobIntegrationForAdv.Services.Simjob
{
    public class JobStageStatus
    {
        public Guid Id { get;  set; }
        public int Seq { get;  set; }
        public string Status { get;  set; }
    }
}
