﻿using Microsoft.AspNetCore.Components.Routing;
using Microsoft.Extensions.Logging.Abstractions;
using Newtonsoft.Json;
using Serilog;
using SimjobIntegrationForAdv.Configurations;
using SimjobIntegrationForAdv.Data.Repositories;
using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Models.Database;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Services.Simjob
{
    public class JobIntegracaoService : IntegracaoService, IJobIntegracaoService
    {
        private readonly IProdutoDatabaseRepository _produtoRepository;
        private readonly IJobRepository _jobSimjobRepository;
        private readonly IResourceRepository _resourceRepository;
        private readonly IStageTypeRepository _stageTypeRepository;
        private readonly IEnvironmentRepository _environmentRepository;
        private readonly IOSDatabaseRepository _oSDatabaseRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IItemRepository _itemRepository;
        private readonly OSConfigurations _oSConfigurations;
        public JobIntegracaoService(ITokenService tokenService, IJobRepository jobSimjobRepository, IOSDatabaseRepository oSDatabaseRepository, OSConfigurations oSConfigurations, IEnvironmentRepository environmentRepository, IAccountRepository accountRepository, IItemRepository itemRepository, IResourceRepository resourceRepository, IStageTypeRepository stageTypeRepository, IProdutoDatabaseRepository produtoRepository) : base(tokenService)
        {
            _jobSimjobRepository = jobSimjobRepository;
            _oSDatabaseRepository = oSDatabaseRepository;
            _oSConfigurations = oSConfigurations;
            _environmentRepository = environmentRepository;
            _accountRepository = accountRepository;
            _itemRepository = itemRepository;
            _resourceRepository = resourceRepository;
            _stageTypeRepository = stageTypeRepository;
            _produtoRepository = produtoRepository;
        }



        private Models.Simjob.Environment GetEnvironmentByConfig(OS os, List<Models.Simjob.Environment> environments)
        {
            //var teste = os.Status == "LIBERADO PARA PROJETOS";
            //if (teste) { }
            string environmentName = _oSConfigurations.StatusEnvironmentConfigurations
                                .FirstOrDefault(s => s.Status == os.Status)?.Environment;
            if (string.IsNullOrEmpty(environmentName)) return null;
            return environments.FirstOrDefault(e => e.Description == environmentName);
        }

        private Resource GetResourceByConfig(OS os, List<Resource> resources)
        {
            string resourceCode = _oSConfigurations.StatusEnvironmentConfigurations
                                .FirstOrDefault(s => s.Status == os.Status)?.ResourceCode;
            if (string.IsNullOrEmpty(resourceCode)) return null;
            return resources.FirstOrDefault(e => e.Code == resourceCode);
        }

        private StageType GetStageTypeByConfig(OS os, List<StageType> stageTypes)
        {
            string stageTypeDescription = _oSConfigurations.StatusEnvironmentConfigurations
                                .FirstOrDefault(s => s.Status == os.Status)?.StageTypeDescription;
            if (string.IsNullOrEmpty(stageTypeDescription)) return null;
            return stageTypes.FirstOrDefault(e => e.Description == stageTypeDescription);
        }


        private string ConvertStatusEnvironment(OS os)
        {
            string sigla = _oSConfigurations.StatusEnvironmentConfigurations
                                .FirstOrDefault(s => s.Status == os.Status)?.Sigla;

            if (string.IsNullOrEmpty(sigla)) return null;

            return $"{os.CodOs}-{os.CodigoProduto}-{sigla}";
        }

        private JobStage CreateJobStage(OS os, List<Models.Simjob.Environment> environments, List<Resource> resources, List<StageType> stageTypes)
        {

            var environment = GetEnvironmentByConfig(os, environments);
            if (environment == null) return null;
            string status = environment.JobStageStatus.OrderBy(s => s.Seq).FirstOrDefault()?.Status;
            Resource resource = GetResourceByConfig(os, resources);
            StageType stageType = GetStageTypeByConfig(os, stageTypes);

            if (resource == null || stageType == null) return null;

            return new JobStage(resource.Type, status, resource.Id, stageType.Id);


        }

        public string RemoveAccents(string text)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = text.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }
            return sbReturn.ToString();
        }

        public async Task InsereJobsNoSimjob()
        {
            try
            {
                var oSs = _oSDatabaseRepository.GetAll();
                var jobs = await _jobSimjobRepository.GetAll();
                var environments = await _environmentRepository.GetAll();

                oSs.ForEach(os => os.Status = RemoveAccents(os.Status));
                environments.ForEach(e => e.Description = RemoveAccents(e.Description));

                //Log.Information("ENVS");
                //Log.Information(JsonConvert.SerializeObject(_oSConfigurations.StatusEnvironmentConfigurations));
                //Log.Information(JsonConvert.SerializeObject(environments));


                //Log.Information("0");
                //Log.Information(JsonConvert.SerializeObject(oSs));

                if (oSs == null || oSs == null || !oSs.Any() || jobs == null) return;

                var oss = from os in oSs
                          from c in _oSConfigurations.StatusEnvironmentConfigurations
                          where os.Status == c.Status
                          select os;

                oss.ToList().ForEach(os => os.StatusSimjob = ConvertStatusEnvironment(os));

                //Log.Information("1");
                //Log.Information(JsonConvert.SerializeObject(oss));

                //oSs.ForEach(os => os.StatusSimjob = ConvertStatusEnvironment(os));
                List<OS> oSsParaIntegrar = oss.Where(o => !jobs.Exists(j => j.Code == o.StatusSimjob)).ToList();

                //Log.Information("2");
                //Log.Information(JsonConvert.SerializeObject(oSsParaIntegrar));
                //oSs.Where(os => os.StatusSimjob != null &&
                //                !jobs.Exists(j => j.Code == os.StatusSimjob))
                //                 .ToList();

                ;   if (!oSsParaIntegrar.Any()) return;

                var accounts = await _accountRepository.GetAll();
                var items = await _itemRepository.GetAll();
                var resources = await _resourceRepository.GetAll();
                var stageTypes = await _stageTypeRepository.GetAll();


                stageTypes.ForEach(st => st.Description = RemoveAccents(st.Description));

                var jobsParaInserir = from os in oSsParaIntegrar
                                      select new Job(os.StatusSimjob,
                                                     os.Produto,
                                                     os.PrevEntrega,
                                                     accounts.FirstOrDefault(account => account.Code == os.CodCli)?.Id,
                                                     GetEnvironmentByConfig(os, environments)?.Id,
                                                      items.FirstOrDefault(item => item.Code == os.CodigoProduto)?.Id,
                                                      new List<JobStage>
                                                      {
                                                          CreateJobStage(os,environments,resources,stageTypes)
                                                      }
                                                     ); ;


                //Log.Information(JsonConvert.SerializeObject(jobsParaInserir));

                jobsParaInserir = jobsParaInserir.Where(j => j.ItemId != null &&
                                                            j.AccountId != null &&
                                                            j.EnvironmentId != null &&
                                                            j.JobStages.Any() &&
                                                            j.JobStages.Exists(js => js != null &&
                                                                                        js.ResourceId != null &&
                                                                                        js.StageTypeId != null &&
                                                                                        !string.IsNullOrEmpty(js.Status) &&
                                                                                         !string.IsNullOrEmpty(js.Description)))
                                                            .ToList();

                if (!jobsParaInserir.Any()) return;
                //return;
                string jobsParaInserirSer = JsonConvert.SerializeObject(jobsParaInserir);

                Log.Information($"JobIntegracaoService - Jobs para inserir: {jobsParaInserirSer}");
                await _jobSimjobRepository.Insert(jobsParaInserir.ToList());

            }
            catch (Exception e)
            {
                Log.Error(e, e.Message);
            }

        }
    }
}
