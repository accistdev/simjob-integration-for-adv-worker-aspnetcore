﻿using SimjobIntegrationForAdv.Configurations;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System;
using Serilog;

namespace SimjobIntegrationForAdv.Services.Simjob
{
    public class TokenService : ITokenService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly SimjobConfigurations _simjobConfigurations;
        private readonly HttpClient _client;
        public TokenService(IHttpClientFactory httpClientFactory, SimjobConfigurations simjobConfigurations)
        {
            _httpClientFactory = httpClientFactory;
            _client = _httpClientFactory.CreateClient("simjob");
            _simjobConfigurations = simjobConfigurations;
        }
        public async Task<TokenContainer> GetToken()
        {
            try
            {
                var model = new { Username = _simjobConfigurations.Username, Password = _simjobConfigurations.Password };
                var json = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var res = await _client.PostAsync("token", content);

                if (!res.IsSuccessStatusCode) return null;

                var body = await res.Content.ReadAsStringAsync();
                var responseDes = JsonConvert.DeserializeObject<ResponseModel<TokenContainer>>(body);
                return responseDes.Data;
            }
            catch (Exception e)
            {
                Log.Error(e, e.Message);
                return null;
            }
        }

        public async Task<HttpClient> GetTokenToHttpClient()
        {
            try
            {
                var model = new { Username = _simjobConfigurations.Username, Password = _simjobConfigurations.Password };
                var json = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                var res = await _client.PostAsync("token", content);

                if (!res.IsSuccessStatusCode) return null;

                var body = await res.Content.ReadAsStringAsync();
                var responseDes = JsonConvert.DeserializeObject<ResponseModel<TokenContainer>>(body);
                var client = _httpClientFactory.CreateClient("simjob");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", responseDes.Data.AccessToken);
                return client;
            }
            catch(Exception e)
            {
                Log.Error(e, e.Message);
                return null;
            }
        }
    }
}
