﻿using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;
using Newtonsoft.Json;
using Serilog;
using System.Linq;
using System.Threading.Tasks;
using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;

namespace SimjobIntegrationForAdv.Services.Simjob
{
    public class AccountIntegracaoService : IntegracaoService, IAccountIntegracaoService
    {
        private readonly IClienteDatabaseRepository _clienteDatabaseRepository;
        private readonly IAccountRepository _accountSimjobRepository;
        public AccountIntegracaoService(ITokenService tokenService, IClienteDatabaseRepository clienteDatabaseRepository, IAccountRepository accountSimjobRepository) : base(tokenService)
        {
            _clienteDatabaseRepository = clienteDatabaseRepository;
            _accountSimjobRepository = accountSimjobRepository;
        }

        public async Task InsereClientesNoSimjob()
        {

            var clientes = _clienteDatabaseRepository.GetAllClientes();
            var accounts = await _accountSimjobRepository.GetAll();

            if (clientes == null || accounts == null || !clientes.Any()) return;

            var clientesParaIntegrar = clientes.Where(cliente => !accounts.Exists(account => cliente.CODCLI == account.Code)).ToList();
            var accountsParaInserir = from cliente in clientesParaIntegrar select new Account { Code = cliente.CODCLI, Name = cliente.RAZAO };

            if (!accountsParaInserir.Any()) return;

            string accountsParaInserirSer = JsonConvert.SerializeObject(accountsParaInserir);
            Log.Information($"AccountIntegracaoService - Accounts para inserir: {accountsParaInserirSer}");
            await _accountSimjobRepository.Insert(accountsParaInserir.ToList());
        }
    }
}
