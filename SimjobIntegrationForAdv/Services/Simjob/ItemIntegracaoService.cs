﻿using Newtonsoft.Json;
using Serilog;
using SimjobIntegrationForAdv.Data.Repositories.Interfaces;
using SimjobIntegrationForAdv.Data.Repositories.Simjob.Interfaces;
using SimjobIntegrationForAdv.Models.Simjob;
using SimjobIntegrationForAdv.Services.Interfaces;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimjobIntegrationForAdv.Services.Simjob
{
    public class ItemIntegracaoService : IntegracaoService, IItemIntegracaoService
    {
        private readonly IProdutoDatabaseRepository _produtoDatabaseRepository;
        private readonly IItemRepository _itemSimjobRepository;
        private readonly IUnitRepository _unitSimjobRepository;
        public ItemIntegracaoService(ITokenService tokenService, IProdutoDatabaseRepository produtoDatabaseRepository, IItemRepository itemSimjobRepository, IUnitRepository unitSimjobRepository) : base(tokenService)
        {

            _produtoDatabaseRepository = produtoDatabaseRepository;
            _itemSimjobRepository = itemSimjobRepository;
            _unitSimjobRepository = unitSimjobRepository;
        }



        public async Task InsereItemsNoSimjob()
        {
            try
            {
                var produtos = _produtoDatabaseRepository.GetAllProdutos();
                var items = await _itemSimjobRepository.GetAll();
                var units = await _unitSimjobRepository.GetAll();

                if (produtos == null || items == null || !produtos.Any()) return;
                var unitId_UN = units?.FirstOrDefault(u => u.Code.ToUpper() == "UN")?.Id;
                var produtosParaIntegrar = produtos.Where(produto => !items.Exists(item => produto.Codigo == item.Code)).ToList();
                var itemsParaInserir = from produto in produtosParaIntegrar
                                       select new Item
                                       {
                                           Code = produto.Codigo,
                                           Description = produto.Descricao,
                                           UnitId = string.IsNullOrEmpty(produto.Un) ? unitId_UN :
                                                           units.FirstOrDefault(u => u.Code.ToUpper() == produto.Un.ToUpper())?.Id
                                       };

                if (!itemsParaInserir.Any()) return;

                string itemsParaInserirSer = JsonConvert.SerializeObject(itemsParaInserir);

                Log.Information($"ItemIntegracaoService - Items para inserir: {itemsParaInserirSer}");
                await _itemSimjobRepository.Insert(itemsParaInserir.ToList());

            }catch(Exception e)
            {
                Log.Error(e, e.Message);
            }

        }


    }
}
