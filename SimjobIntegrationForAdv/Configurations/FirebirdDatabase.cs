﻿namespace SimjobIntegrationForAdv.Configurations
{
    public class FirebirdDatabase
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
