﻿namespace SimjobIntegrationForAdv.Configurations
{
    public class StatusEnvironmentConfiguration
    {
        public string Status { get; set; }
        public string Environment { get; set; }
        public string Sigla { get; set; }
        public string ResourceCode { get; set; }
        public string StageTypeDescription { get; set; }
    }
}
