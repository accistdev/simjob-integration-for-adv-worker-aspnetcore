﻿using System.Collections.Generic;

namespace SimjobIntegrationForAdv.Configurations
{
    public class OSConfigurations
    {
        public List<StatusEnvironmentConfiguration> StatusEnvironmentConfigurations { get; set; }
    }
}
