﻿namespace SimjobIntegrationForAdv.Configurations
{
    public class WorkerConfigurations
    {
        public int InsereClientesSimjobWorker_Intervalo_Segundos { get; set; }
        public int InsereProdutosSimjobWorker_Intervalo_Segundos { get; set; }
        public int InsereJobsSimjobWorker_Intervalo_Segundos { get; set; }
    }
}
