using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace SimjobIntegrationForAdv
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var windowsService = bool.Parse(configuration.GetSection("WindowsService").Value);
            string port = configuration.GetSection("Port").Value;
            string pathLog = Path.Join(configuration["Logging:Path"] ?? Directory.GetCurrentDirectory(), "log.txt");
            Log.Logger = new LoggerConfiguration()
               .WriteTo.Console()
               .WriteTo.File(pathLog, retainedFileCountLimit: 2, rollingInterval: RollingInterval.Month)

               .CreateLogger();
            try
            {

                if (windowsService)
                    return Host.CreateDefaultBuilder(args)
                      .UseWindowsService()
                      .ConfigureWebHostDefaults(webBuilder =>
                      {
                          webBuilder.UseStartup<Startup>();
                          webBuilder.UseUrls($"http://*:{port}");
                      }).UseSerilog();

                return Host.CreateDefaultBuilder(args)
                       .ConfigureWebHostDefaults(webBuilder =>
                       {
                           webBuilder.UseStartup<Startup>();
                           webBuilder.UseUrls($"http://*:{port}");
                       }).UseSerilog();
            }
            catch (Exception e)
            {
                Log.Fatal(e, e.Message);
                throw;
            }
        }
    }
}
